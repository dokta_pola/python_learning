class Human:
    def __init__(self, name, hair, eyes, tattoo, body, sex):
        self.name = name
        self.hair = hair
        self.eyes = eyes
        self.tattoo = tattoo
        self.body_type = body
        self.sex = sex

    def say_hello(self):
        print('Hello, my name is ' + self.name)

    def run(self):
        print("I walked 2 steps")
