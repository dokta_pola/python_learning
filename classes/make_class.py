class User:

    def set_name(self, name):
        self.name = name

    def print(self):
        print('My name:', self.name)


u = User()
u.set_name('Ivan')
u.print()
