class Bag:
    def __init__(self):
        self.array = []

    def add(self, number):
        self.array.append(number)

    def empty(self):
        if len(self.array) == 0:
            return True
        else:
            return False

    def size(self):
        return len(self.array)


b = Bag()
b.add(6)
b.add(67)
print(b.size())