from get_middle import get_middle

array = [92, 323, 1500, 2302, 6098, 7660]
number_to_find = int(input('Print your number:'))


def binary_search(num, arr):
    low = 0
    high = len(arr) - 1
    while low < high:
        half = get_middle(arr, 0, len(arr) - 1)
        if num < half:
            # half = get_middle(arr, low, high // 2)
            high = arr[half] - 1
            # high = arr[half] - 1
        elif num > half:
            high = arr[half] + 1
            # half = get_middle(arr, (len(arr) - 1) // 2, len(arr) - 1)
            # half = arr[half] + 1
        elif num == arr[half]:
            return 'not found'
        return half


print(binary_search(number_to_find, array))
