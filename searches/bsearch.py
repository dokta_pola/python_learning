array = [1, 94, 667, 1000, 2356]
Num = int(input())


def binary_search(num, arr):
    low = 0
    high = len(arr) - 1
    while low <= high:
        half = (low + high) // 2
        if num < arr[half]:
            high = half - 1
        elif num > arr[half]:
            low = half + 1
        elif num == arr[half]:
            return arr[half]
    return 'not found'



print(binary_search(Num, array))
