array = [1, 94, 667, 1000, 2356]
Num = int(input())


def binary_search(num, arr, low, high):
    if low <= high:
        half = (low + high) // 2
        if num < arr[half]:
            return binary_search(num, arr, low, half - 1)
        elif num > arr[half]:
            return binary_search(num, arr, half + 1, high)
        # elif num == arr[half]:
        else:
            return arr[half]
    else:
        return 'not found'


print(binary_search(Num, array, 0, len(array) - 1))
