import unittest
from selection_sort1 import selection_sort


class TestSelection_Sort(unittest.TestCase):
    def test_sort_ideal(self):
        arr = [9, 6, 54, 0, 765]
        expected_res = [765, 54, 9, 6, 0]
        result = selection_sort(selection_sort(arr))
        for i in range(0, len(expected_res)):
            self.assertEqual(result[i], expected_res[i])

    def test_sort_double_int(self):
        arr = [9, 0, 54, 9, 0, 765]
        expected_res = [765, 54, 9, 9, 0, 0]
        result = selection_sort(selection_sort(arr))
        for i in range(0, len(expected_res)):
            self.assertEqual(result[i], expected_res[i])

    def test_already_sorted(self):
        arr = [108, 54, 9, 6, 0]
        expected_res = [108, 54, 9, 6, 0]
        result = selection_sort(selection_sort(arr))
        for i in range(0, len(expected_res)):
            self.assertEqual(result[i], expected_res[i])

    def test_one_el(self):
        arr = [6]
        expected_res = [6]
        result = selection_sort(selection_sort(arr))
        for i in range(0, len(expected_res)):
            self.assertEqual(result[i], expected_res[i])

    def test_empty_array(self):
        self.assertEqual(selection_sort([]), 'array is empty')


if __name__ == '__main__':
    unittest.main()
