import unittest

from celsius_to_f import CelsiusToFahrenheit


class TestCelsius_to_F(unittest.TestCase):
    def test_CelsiusToFahrenheit(self):
        self.assertEqual(CelsiusToFahrenheit(50), 59.77777777777778)

    def test_CelsiusToFahrenheit_str(self):
        self.assertEqual(CelsiusToFahrenheit('a'), 'Print pls Integer!')

    def test_CelsiusToFahrenheit_arr(self):
        self.assertEqual(CelsiusToFahrenheit([]), 'Print pls Integer!')

    def test_CelsiusToFahrenheit_dict(self):
        self.assertEqual(CelsiusToFahrenheit({}), 'Print pls Integer!')


if __name__ == '__main__':
    unittest.main()
