array = [1, 100]
even_array = []
odd_array = []
event_array = []

for eachNum in range(1, 101):
    if eachNum % 2 == 0:
        even_array.append(eachNum)
    elif eachNum % 3 == 0:
        event_array.append(eachNum)
    else:
        odd_array.append(eachNum)

print(even_array)
print(odd_array)
print(event_array)
