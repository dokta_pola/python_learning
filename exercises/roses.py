def rose_or_roses(num_roses):  # function
    if num_roses != 1:  # if variable is not equal one(it should be > 10 , program returns 'roses'
        return 'roses'
    return 'rose'  # if there is only one rose, program returns 'rose'


def roses_left(roses_count):  # this function shows us, how many roses we have at the moment
    for i in range(roses_count, -1, -1):  # it's a loop, program count from the last variable to the first
        print(i, rose_or_roses(i), 'left')  # output


roses_left(9)  # function call & input


def roses_left_rec(roses_count):  # recursion
    print(roses_count, 'left')
    if roses_count == 0:
        return
    roses_left_rec(roses_count - 1)


roses_left_rec(9)
