def reverse_list(my_list):
    my_list2 = []
    x = len(my_list) - 1
    while x >= 0:
        my_list2.append(my_list[x])
        x -= 1
    return my_list2


print(reverse_list([1, 'd', 2, 3, 'c', 55]))
