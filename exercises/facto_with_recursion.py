factorial = int(input('Pls, print factorial: '))


def fact(num):  # <----- function which counts factorial without recursion
    if num == 1:  # <------ particular case
        return 1
    elif num < 0:  # <-------- restrictions, because factorial can't be negative
        print('must be positive')
    else:
        number = 1  # <------ here we start to count factorial by using usiual method
        for i in range(num):
            number = number * (i + 1)
        return number


def fact_rec(num):  # <-------function which counts factorial by using recursion
    if num == 1:  # <--------- base case
        return 1
    elif num < 1:  # <-------- restrictions, because factorial can't be negative
        return None
    else:
        return num * fact_rec(num - 1)  # <----- general case, when function calles itself


print(fact_rec(factorial))
