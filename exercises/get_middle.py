array = [1, 2, 3, 4, 5, 6, 7, 8, 9]


def get_middle(arr, low, high):
    return arr[low + (high - low)//2]


print(get_middle(array, 0, len(array) - 1))
print(get_middle(array, 0, 1))
print(get_middle(array, 0, 2))
print(get_middle(array, 6, len(array) - 1))
d = get_middle(array, 6, 6)
print(d)