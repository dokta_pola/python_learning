my_list = [1, 2, 3, 4, 5, 6]

for eachnum in my_list:
    if eachnum % 3 == 0 or eachnum % 2 == 0:
        print(eachnum)

for eachnum in my_list:
    if eachnum % 3 == 0 and eachnum % 2 == 0:
        print(eachnum)

for eachnum in my_list:
    if not eachnum % 2 == 0:
        print(eachnum)
