my_list = ['1', 'd', '9', 'n', 'k']


def transform(lst):
    for even in range(0, len(lst), 2):
        lst[even] = str(even)
    for odd in range(1, len(lst), 2):
        lst[odd] = lst[odd].upper()
    lst.insert(0, '<')
    lst.append('>')
    return lst


print(transform(my_list))
