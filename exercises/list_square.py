# Write a Python function to create and print a list where the values are square of numbers between 1 and 30 (both included)

my_array = []


def listSquare(my_array):
    for x in range(1, 31):
        my_array.append(x ** 2)
    return my_array


print(listSquare(my_array))
