# Write a Python function to sum all the numbers in a list.
# Sample List : [8, 2, 3, 0, 7]
# Expected Output 20


def sum_inside_list(sum):
    zSum = 0
    for i in sum:
        zSum += i
    return zSum


print(sum_inside_list([8, 2, 3, 0, 7]))
