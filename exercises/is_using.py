my_list = [0, 1, 30]


def func(lst):
    lst[1] = 100
    return lst


print(func(my_list))

if func(my_list) is my_list:
    print('true')
else:
    print('false')
