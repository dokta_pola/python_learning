letters = ['a', 'b', 'd', 'h']


# python style
for letter in letters:
    print(letter)

# classic
for index in range(0, len(letters)):
    print(letters[index])