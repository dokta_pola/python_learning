def transform(file_path):
    fi = open(file_path, 'r')
    lines = [line.strip() for line in fi]
    fi.close()
    array = []
    for line in lines:
        line = line.split()
        array.append(line)
    return array


def selection_sort(arr):
    for num1 in range(len(arr)):
        maxi = num1
        for num2 in range(num1 + 1, len(arr)):
            if arr[num2] > arr[maxi]:
                maxi = num2
        arr[num1], arr[maxi] = arr[maxi], arr[num1]
    return arr


def write_file(sorted_array, file_path):
    f = open(file_path, 'a+')
    f.write('\n')
    for i in sorted_array:
        f.write(str(i) + '\t')
    f.close()


data = transform('C:\\Users\\Полина\\Desktop\\i_file.txt')
for arr in data:
    sorted_data = selection_sort(arr)
    print(sorted_data)
    write_file(sorted_data, 'C:\\Users\\Полина\\Desktop\\new_file.txt')
