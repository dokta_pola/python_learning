def transform(file_path):
    fi = open(file_path, 'r')
    lines = fi.read().splitlines()
    fi.close()
    my_array = []
    for line in lines:
        tmp = line.split(' ')
        for el in tmp:
            my_array.append(int(el))
    return my_array


def selection_sort(arr):
    for num1 in range(len(arr)):
        maxi = num1
        for num2 in range(num1 + 1, len(arr)):
            if arr[num2] > arr[maxi]:
                maxi = num2
        arr[num1], arr[maxi] = arr[maxi], arr[num1]
    return arr


def write_file(sorted_array, file_path):
    f = open(file_path, 'a+')
    f.write('\n')
    for i in sorted_array:
        f.write(str(i) + '\t')
    f.close()


data = transform('C:\\Users\\Полина\\Desktop\\data.txt')
sorted_data = selection_sort(data)
print(sorted_data)
write_file(sorted_data, 'C:\\Users\\Полина\\Desktop\\data1.txt')
