array = [14,14]


def is_sorted(arr):
    flag = False
    flag1 = False
    for i in range(len(arr) - 1):  # iterative control structure
        if arr[i] < arr[i + 1]:  # standard conditional structure
            flag = True  # boolean expression
        elif arr[i] > arr[i + 1]:
            flag1 = True
        elif arr[i] == arr[i + 1]:
            flag1 = True
    return flag ^ flag1


print(is_sorted(array))
