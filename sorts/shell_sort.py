import random

array = random.sample(range(-1000, 1000), 1000)


def shell_sort(arr):
    h = len(arr) // 2
    while h >= 1:
        for num in range(h, len(arr)):
            while num - h >= 0 and arr[num] < arr[num - h]:
                arr[num], arr[num - h] = arr[num - h], arr[num]
                num -= h
        h = h // 2
    return arr


print(shell_sort(array))

if sorted(array) == array:
    print("Sorted")
else:
    print("Not sorted")
